import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-no-comunes',
  templateUrl: './no-comunes.component.html'
})
export class NoComunesComponent implements OnInit {
  //Select
  nombre: string = 'Carlos';
  genero: string = 'masculino';

  invitacionMapa= {
    'masculino': 'invitarlo',
    'femenino': 'invitarla'
  };

  //Plural

  cliente: string[] = ['Maria', 'Juan', 'Pedro', 'Samuel', 'Alvarado', 'Carlos']
  clientesMap = {
    '=0': 'no tenemos ningun cliente esperando',
    '=1': 'tenemos un cliente esperando',
    'other': 'Tenemos # clientes esperando'
  }

  constructor() { }

  ngOnInit(): void {
  }

  cambiarCliente() {
    this.nombre = 'Maria';
    this.genero = 'femenino';
  }

  borrarCliente() {
    this.cliente.pop();
  }

  // KeyValue Pipes

  persona = {
    nombre: 'Carlos',
    edad: 35,
    direccion: 'Ciudad de Guatemala, Guatemala'
  }

  //Json Pipes

  heroes = [
    {
      nombre: 'Superman',
      vuela: true
    },
    {
      nombre: 'Robin',
      vuela: false
    },
    {
      nombre: 'Aqueman',
      vuela: false
    }
  ]

  //Async Pipe

  miObservable = interval(1000)

  valorPromesa = new Promise((resolve, reject) => {

    setTimeout(() => {
      resolve('Tenemos data de promesa')
    }, 3500)

  })

}
